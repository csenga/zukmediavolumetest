package mediavolumetest.csenga.hu.mediavolumetest;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import java.io.File;

public class MyService extends Service {


    public static String START_MUSIC = "START";


    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        if (intent.getAction().equals(START_MUSIC)) {

            startToPlay();
        }
        return super.onStartCommand(intent, flags, startId);
    }


    boolean first = true;

    public void startToPlay() {
        Uri uri = Uri.parse("android.resource://mediavolumetest.csenga.hu.mediavolumetest/" + R.raw.test2);
        try {

            AppContext.player.reset();
            AppContext.player.setDataSource(this, uri);

            AppContext.player.prepare();

            AppContext.player.start();

            AppContext.player.seekTo(200000);

            if (first == true) {


                showNotificationLPBug();
                // showNotificationLPNoBug();
                first = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static final int NOTIFICATION_ID = -999911;

    private void showNotificationLPBug() {
        createNotificationLP(true);

        this.startForeground(NOTIFICATION_ID, mNotification);
    }

    private void showNotificationLPNoBug() {

        Bitmap icon = null;


        Notification.Builder builder = new Notification.Builder(this)


                .setLargeIcon(icon)


                .setContentText("artist")

                .setContentIntent(retreivePlaybackAction(4))
                .setContentTitle("title");


        builder.setOngoing(false);


        mNotification = builder.build();

        this.startForeground(NOTIFICATION_ID, mNotification);
    }


    Bitmap mArtwork;
    MediaSession mMediaSession;

    Notification mNotification;


    //HANDLES MEDIA BUTTON LP+
    private static Object mMediaSessionCallback = null;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void createNotificationLP(boolean isPlaying) {

        mArtwork = null;

        mMediaSession = new MediaSession(this, "TAG");


        mMediaSession.setMetadata(new MediaMetadata.Builder()
                .putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART, mArtwork)
                .putString(MediaMetadata.METADATA_KEY_ARTIST, "artist")
                .putString(MediaMetadata.METADATA_KEY_TITLE, "title")
                .putString(MediaMetadata.METADATA_KEY_ALBUM, "album")
                .build());

        if (mMediaSession == null) {

            mMediaSessionCallback = new MediaSession.Callback() {

                @Override
                public void onPause() {
                    super.onPause();
                }

                @Override
                public void onPlay() {
                    super.onPlay();
                }

                @Override
                public void onStop() {
                    super.onStop();
                }

                @Override
                public void onSkipToNext() {
                    super.onSkipToNext();
                }

                @Override
                public void onSkipToPrevious() {
                    super.onSkipToPrevious();
                }
            };
        }

        mMediaSession.setCallback((MediaSession.Callback) mMediaSessionCallback);

        mMediaSession.setFlags(MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS | MediaSession.FLAG_HANDLES_MEDIA_BUTTONS);


        int playbackState;

        if (isPlaying) {
            playbackState = PlaybackState.STATE_PLAYING;
        } else {
            playbackState = PlaybackState.STATE_PAUSED;
        }

        PlaybackState state = new PlaybackState.Builder()
                .setActions(
                        PlaybackState.ACTION_PLAY | PlaybackState.ACTION_PLAY_PAUSE |
                                PlaybackState.ACTION_PLAY_FROM_MEDIA_ID | PlaybackState.ACTION_PAUSE |
                                PlaybackState.ACTION_SKIP_TO_NEXT | PlaybackState.ACTION_SKIP_TO_PREVIOUS)
                .setState(playbackState, PlaybackState.PLAYBACK_POSITION_UNKNOWN, 1, SystemClock.elapsedRealtime())
                .build();
        mMediaSession.setPlaybackState(state);


        mMediaSession.setActive(true);


        int playPauseResource;

        if (isPlaying) {
            playPauseResource = android.R.drawable.ic_media_pause;
        } else {
            playPauseResource = android.R.drawable.ic_media_play;
        }


        Bitmap icon = null;


        Notification.Builder builder = new Notification.Builder(this)

                .setShowWhen(false)

                .setStyle(new Notification.MediaStyle()


                        .setMediaSession(mMediaSession.getSessionToken())

                        .setShowActionsInCompactView(0, 1, 2))

                        // .setColor(0xFFDB4437)

                .setLargeIcon(icon)


                .setContentText("artist")

                .setContentIntent(retreivePlaybackAction(4))
                .setContentTitle("title")


                .addAction(android.R.drawable.ic_media_previous, "prev", retreivePlaybackAction(3))
                .addAction(playPauseResource, "pause", retreivePlaybackAction(1))
                .addAction(android.R.drawable.ic_media_next, "next", retreivePlaybackAction(2));


        builder.setOngoing(true);


        mNotification = builder.build();


    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private PendingIntent retreivePlaybackAction(int which) {
        Intent action;
        PendingIntent pendingIntent;
        final ComponentName serviceName = new ComponentName(this, MyService.class);

        action = new Intent("a");
        action.setComponent(serviceName);
        pendingIntent = PendingIntent.getService(this, 1, action, 0);
        return pendingIntent;
    }

}
