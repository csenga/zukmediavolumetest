package mediavolumetest.csenga.hu.mediavolumetest;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

/**
 * Created by Ragnar on 2015. 11. 16..
 */
public class AppContext extends Application {

    public static Context cntx;
    public static MediaPlayer player;

    @Override
    public void onCreate() {

        super.onCreate();

        cntx=getApplicationContext();

        player=new MediaPlayer();


        player.setWakeMode(this,
                PowerManager.PARTIAL_WAKE_LOCK);

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                Intent i=new Intent(AppContext.this,MyService.class);

                i.setAction(MyService.START_MUSIC);
                startService(i);
            }
        });



    }

}
